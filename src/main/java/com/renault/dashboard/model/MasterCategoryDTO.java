/**
 * 
 */
package com.renault.dashboard.model;

/**
 * @author z023225
 *
 */
public class MasterCategoryDTO {

    private String category;
    private String Type;
    private String group_by;
	
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return Type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		Type = type;
	}
	/**
	 * @return the group_by
	 */
	public String getGroup_by() {
		return group_by;
	}
	/**
	 * @param group_by the group_by to set
	 */
	public void setGroup_by(String group_by) {
		this.group_by = group_by;
	}
    
    
}
