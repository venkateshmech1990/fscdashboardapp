package com.renault.dashboard.model;

import com.renault.dashboard.entity.MasterCategoryEntity;

/**
 * @author z022411
 *
 */
public class MasterDataDTO {
	
	private MasterCategoryEntity masterCategoryEntity ;
	private String domain;
	private String product;
	private String week;
	private String value;
	private byte[] image;
	
	/**
	 * @return the masterCategoryEntity
	 */
	public MasterCategoryEntity getMasterCategoryEntity() {
		return masterCategoryEntity;
	}
	/**
	 * @param masterCategoryEntity the masterCategoryEntity to set
	 */
	public void setMasterCategoryEntity(MasterCategoryEntity masterCategoryEntity) {
		this.masterCategoryEntity = masterCategoryEntity;
	}
	/**
	 * @return the domain
	 */
	public String getDomain() {
		return domain;
	}
	/**
	 * @param domain the domain to set
	 */
	public void setDomain(String domain) {
		this.domain = domain;
	}
	/**
	 * @return the product
	 */
	public String getProduct() {
		return product;
	}
	/**
	 * @param product the product to set
	 */
	public void setProduct(String product) {
		this.product = product;
	}
	/**
	 * @return the week
	 */
	public String getWeek() {
		return week;
	}
	/**
	 * @param week the week to set
	 */
	public void setWeek(String week) {
		this.week = week;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	/**
	 * @return the image
	 */
	public byte[] getImage() {
		return image;
	}
	/**
	 * @param image the image to set
	 */
	public void setImage(byte[] image) {
		this.image = image;
	}
}
