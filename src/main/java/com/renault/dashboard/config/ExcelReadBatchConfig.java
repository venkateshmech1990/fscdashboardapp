package com.renault.dashboard.config;

import org.modelmapper.ModelMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;

import com.renault.dashboard.Listener.JobCompletionListener;
import com.renault.dashboard.batch.processor.ExcelMasterDataProcessor;
import com.renault.dashboard.batch.processor.ExcelProcessor;
import com.renault.dashboard.batch.writer.DataWriter;
import com.renault.dashboard.batch.writer.MasterDataWriter;
import com.renault.dashboard.excel.RowMapper;
import com.renault.dashboard.excel.poi.PoiItemReader;
import com.renault.dashboard.mapper.MasterDataRowMapper;
import com.renault.dashboard.mapper.MasterExcelRowMapper;
import com.renault.dashboard.model.MasterCategoryDTO;
import com.renault.dashboard.model.MasterDataDTO;

@Configuration
@EnableBatchProcessing
public class ExcelReadBatchConfig {

//	@Autowired
//    private JobBuilderFactory jobBuilderFactory;
// 
//    @Autowired
//    private StepBuilderFactory stepBuilderFactory;

	private static final String PROPERTY_EXCEL_SOURCE_FILE_PATH = "excel.to.database.job.source.file.path";

//    @Bean
//	public Job processJob() {
//		return jobBuilderFactory.get("processJob")
//				.incrementer(new RunIdIncrementer()).listener(listener())
//				.flow(excelInsert()).end().build();
//	}
//
//	@Bean
//	public Step excelInsert() {
//		return stepBuilderFactory.get("excelInsert").<String, String> chunk(1)
//				.reader(new ExcelReader()).processor(new ExcelProcessor())
//				.writer(new DataWriter()).build();
//	}
//
//	@Bean
//	public JobExecutionListener listener() {
//		return new JobCompletionListener();
//	}

	@Bean
	ItemReader<MasterCategoryDTO> excelStudentReader(Environment environment) {
		PoiItemReader<MasterCategoryDTO> reader = new PoiItemReader<>();
		reader.setLinesToSkip(1);
		reader.setResource(new ClassPathResource(environment.getRequiredProperty(PROPERTY_EXCEL_SOURCE_FILE_PATH)));
		reader.setRowMapper(excelRowMapper());
		return reader;
	}

//    private RowMapper<MasterCategoryDTO> excelRowMapper() {
//        BeanWrapperRowMapper<MasterCategoryDTO> rowMapper = new BeanWrapperRowMapper<>();
//        rowMapper.setTargetType(MasterCategoryDTO.class);
//        return rowMapper;
//    }

	/**
	 * If your Excel document has no header, you have to create a custom row mapper
	 * and configure it here.
	 */
	private RowMapper<MasterCategoryDTO> excelRowMapper() {
		return new MasterExcelRowMapper();
	}

	@Bean
	ItemProcessor<MasterCategoryDTO, MasterCategoryDTO> excelStudentProcessor() {
		return new ExcelProcessor();
	}

	@Bean
	ItemWriter<MasterCategoryDTO> excelStudentWriter() {
		return new DataWriter();
	}

	@Bean
	Step excelFileToDatabaseStep(ItemReader<MasterCategoryDTO> excelStudentReader,
			ItemProcessor<MasterCategoryDTO, MasterCategoryDTO> excelStudentProcessor,
			ItemWriter<MasterCategoryDTO> excelStudentWriter, StepBuilderFactory stepBuilderFactory) {
		return stepBuilderFactory.get("excelFileToDatabaseStep").<MasterCategoryDTO, MasterCategoryDTO>chunk(3)
				.reader(excelStudentReader).processor(excelStudentProcessor).writer(excelStudentWriter).build();
	}

	@Bean
	Job excelFileToDatabaseJob(JobBuilderFactory jobBuilderFactory,
			@Qualifier("excelFileToDatabaseStep") Step excelStudentStep) {
		return jobBuilderFactory.get("excelFileToDatabaseJob").incrementer(new RunIdIncrementer())
				.flow(excelStudentStep).end().build();
	}

	@Bean
	public JobExecutionListener listener() {
		return new JobCompletionListener();
	}
	
	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}
	
	@Bean
	ItemReader<MasterDataDTO> excelMasterDataReader(Environment environment) {
		PoiItemReader<MasterDataDTO> reader = new PoiItemReader<>();
		reader.setLinesToSkip(1);
		reader.setResource(new ClassPathResource(environment.getRequiredProperty(PROPERTY_EXCEL_SOURCE_FILE_PATH)));
		reader.setRowMapper(masterDataRowMapper());
		return reader;
	}
	
	/**
	 * If your Excel document has no header, you have to create a custom row mapper
	 * and configure it here.
	 */
	private RowMapper<MasterDataDTO> masterDataRowMapper() {
		return new MasterDataRowMapper();
	}
	
	@Bean
	ItemProcessor<MasterDataDTO, MasterDataDTO> excelMasterDataProcessor() {
		return new ExcelMasterDataProcessor();
	}

	@Bean
	ItemWriter<MasterDataDTO> excelMasterDatatWriter() {
		return new MasterDataWriter();
	}
	
	@Bean
	Step excelFileToMasterDataStep(ItemReader<MasterDataDTO> excelMasterDataReader,
			ItemProcessor<MasterDataDTO, MasterDataDTO> excelMasterDataProcessor,
			ItemWriter<MasterDataDTO> excelMasterDatatWriter, StepBuilderFactory stepBuilderFactory) {
		return stepBuilderFactory.get("excelFileToDatabaseStep").<MasterDataDTO, MasterDataDTO>chunk(3)
				.reader(excelMasterDataReader).processor(excelMasterDataProcessor).writer(excelMasterDatatWriter).build();
	}

	@Bean
	Job excelFileToMasterDataJob(JobBuilderFactory jobBuilderFactory,
			@Qualifier("excelFileToMasterDataStep") Step excelMasterDataStep) {
		return jobBuilderFactory.get("excelFileToMasterDataJob").incrementer(new RunIdIncrementer())
				.flow(excelMasterDataStep).end().build();
	}

}
