package com.renault.dashboard.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.renault.dashboard.entity.MasterDataEntity;

/**
 * @author z022411
 *
 */
public interface MasterDataRepository extends CrudRepository<MasterDataEntity, Long> {
	@Query("select count(*) as datacount from MasterDataEntity mde where mde.domain = :domain AND mde.product = :product")
	int findByDomainandProduct(String domain, String product);

}
