/**
 * 
 */
package com.renault.dashboard.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.renault.dashboard.entity.MasterCategoryEntity;

/**
 * @author z023225
 *
 */
public interface MasterRepository extends CrudRepository<MasterCategoryEntity, Long> {
	
	@Query("select count(*) as datacount from MasterCategoryEntity mce where mce.category = :category AND mce.Type = :type")
	int findByCategoryandType(String category, String type);

}
