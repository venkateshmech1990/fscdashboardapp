/**
 * 
 */
package com.renault.dashboard.mapper;

import com.renault.dashboard.excel.RowMapper;
import com.renault.dashboard.excel.support.rowset.RowSet;
import com.renault.dashboard.model.MasterCategoryDTO;

/**
 * @author z023225
 *
 */
public class MasterExcelRowMapper implements RowMapper<MasterCategoryDTO> {

    @Override
    public MasterCategoryDTO mapRow(RowSet rowSet) throws Exception {
    	MasterCategoryDTO masterDto = new MasterCategoryDTO();
    	masterDto.setCategory(rowSet.getColumnValue(0));
    	masterDto.setType(rowSet.getColumnValue(1));
    	masterDto.setGroup_by(rowSet.getColumnValue(2));

        return masterDto;
    }
}
