/**
 * 
 */
package com.renault.dashboard.mapper;

import com.renault.dashboard.excel.RowMapper;
import com.renault.dashboard.excel.support.rowset.RowSet;
import com.renault.dashboard.model.MasterDataDTO;

/**
 * @author z022411
 *
 */
public class MasterDataRowMapper implements RowMapper<MasterDataDTO> {

    @Override
    public MasterDataDTO mapRow(RowSet rowSet) throws Exception {
    	MasterDataDTO masterDto = new MasterDataDTO();
    	//masterDto.setMasterCategoryEntity(rowSet.getColumnValue(0));
    	masterDto.setDomain(rowSet.getColumnValue(0));
    	masterDto.setProduct(rowSet.getColumnValue(1));
    	masterDto.setWeek(rowSet.getColumnValue(2));
    	masterDto.setValue(rowSet.getColumnValue(3));
    	//masterDto.setImage(rowSet.getColumnValue(4));

        return masterDto;
    }
}
