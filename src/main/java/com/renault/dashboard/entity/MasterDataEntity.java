package com.renault.dashboard.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author z022411
 *
 */
@Entity
@Table(name = "master_cdata")
public class MasterDataEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "master_category")
	@JoinColumn(name = "category_id", referencedColumnName = "id")
	private MasterCategoryEntity masterCategoryEntity ;

	private String domain;
	private String product;
	private String week;
	private String value;
	private byte[] image;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public MasterCategoryEntity getMasterCategoryEntity() {
		return masterCategoryEntity;
	}
	public void setMasterCategoryEntity(MasterCategoryEntity masterCategoryEntity) {
		this.masterCategoryEntity = masterCategoryEntity;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getWeek() {
		return week;
	}
	public void setWeek(String week) {
		this.week = week;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}  
}
