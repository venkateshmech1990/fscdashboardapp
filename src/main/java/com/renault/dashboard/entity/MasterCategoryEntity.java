/**
 * 
 */
package com.renault.dashboard.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author z023225
 *
 */
@Entity
@Table(name = "master_category")
public class MasterCategoryEntity {

	@Id
	@GeneratedValue
    private Long id;
    private String category;
    private String Type;
    private String group_by;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return Type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		Type = type;
	}
	/**
	 * @return the group_by
	 */
	public String getGroup_by() {
		return group_by;
	}
	/**
	 * @param group_by the group_by to set
	 */
	public void setGroup_by(String group_by) {
		this.group_by = group_by;
	}
    
    
}
