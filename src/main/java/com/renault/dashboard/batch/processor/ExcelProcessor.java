/**
 * 
 */
package com.renault.dashboard.batch.processor;

import org.springframework.batch.item.ItemProcessor;

import com.renault.dashboard.model.MasterCategoryDTO;

/**
 * @author z023225
 *
 */
public class ExcelProcessor implements ItemProcessor<MasterCategoryDTO, MasterCategoryDTO> {

	@Override
    public MasterCategoryDTO process(MasterCategoryDTO item) throws Exception {
        return item;
    }
}