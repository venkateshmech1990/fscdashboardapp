/**
 * 
 */
package com.renault.dashboard.batch.processor;

import org.springframework.batch.item.ItemProcessor;

import com.renault.dashboard.model.MasterDataDTO;

/**
 * @author z022411
 *
 */
public class ExcelMasterDataProcessor implements ItemProcessor<MasterDataDTO, MasterDataDTO> {

	@Override
    public MasterDataDTO process(MasterDataDTO item) throws Exception {
        return item;
    }
}