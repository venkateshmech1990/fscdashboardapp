/**
 * 
 */
package com.renault.dashboard.batch.writer;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.renault.dashboard.constants.util.ObjectMapperUtils;
import com.renault.dashboard.entity.MasterCategoryEntity;
import com.renault.dashboard.entity.MasterDataEntity;
import com.renault.dashboard.model.MasterDataDTO;
import com.renault.dashboard.repository.MasterDataRepository;
import com.renault.dashboard.repository.MasterRepository;

/**
 * @author z022411
 *
 */
public class MasterDataWriter implements ItemWriter<MasterDataDTO> {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	MasterDataRepository masterDataRepository;

	 @Override
	    public void write(List<? extends MasterDataDTO> items) throws Exception {
	      List<MasterDataDTO> masterDataList= new ArrayList<MasterDataDTO>();

	        items.forEach(masterData -> {
	        	int count = masterDataRepository.findByDomainandProduct(masterData.getDomain(), masterData.getProduct());
	        	if(count == 0) {
	        		masterDataList.add(masterData);
	        	}
	        	
	        });
	        if(masterDataList.size() >0) {
		        List<MasterDataEntity> masterDataEntity = ObjectMapperUtils.mapAll(masterDataList,MasterDataEntity.class);
		        masterDataRepository.saveAll(masterDataEntity);
		        LOGGER.info("Insertion of database completed successfully");
	        }else {
	        	LOGGER.info("Duplicate data identified and not inserted");
	        }
	        
	    }

}
