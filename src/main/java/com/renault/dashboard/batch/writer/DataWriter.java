/**
 * 
 */
package com.renault.dashboard.batch.writer;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.renault.dashboard.constants.util.ObjectMapperUtils;
import com.renault.dashboard.entity.MasterCategoryEntity;
import com.renault.dashboard.model.MasterCategoryDTO;
import com.renault.dashboard.repository.MasterRepository;

/**
 * @author z023225
 *
 */
public class DataWriter implements ItemWriter<MasterCategoryDTO> {
	
	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	@Autowired
	MasterRepository masterRepository;

	 @Override
	    public void write(List<? extends MasterCategoryDTO> items) throws Exception {
		 LOGGER.info("Insertion of database starts");
	      List<MasterCategoryDTO> masterList= new ArrayList<MasterCategoryDTO>();

	        for(MasterCategoryDTO masterdata: items) {
	        	int datacount=masterRepository.findByCategoryandType(masterdata.getCategory(), masterdata.getType());
	        	if(datacount==0) {
	        		masterList.add(masterdata);
	        	}
	        }
	        if(masterList.size()>0) {
	        List<MasterCategoryEntity> masterCategoryEntity = ObjectMapperUtils.mapAll(masterList,MasterCategoryEntity.class);
	        masterRepository.saveAll(masterCategoryEntity);
	        LOGGER.info("Insertion of database ends");
	        }else {
	        	 LOGGER.info("Unique data identified and not inserted");
	        }
	        
	    }

}
